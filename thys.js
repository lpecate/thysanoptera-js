// See: https://gitlab.com/sliceo/websites/thysanoptera-js
(function() {
  var size = 4;
  var number = 1;
  var max_number = 30;
  var speed = 7; // pixels / sec
  var DEBUG = false;

  let style = document.createElement('style');
  style.setAttribute('type', 'text/css');
  style.innerHTML = `
  .thyses {
    position: fixed;
    top: 0;
    left: 0;
    margin: 0;
    width: calc(100vw - 17px);
    height: 100vh;
    overflow: hidden;
    pointer-events: none;
  }
  .thys {
    opacity: 0;
    top: 0;
    left: 0;
    width: 5px;
    height: 1px;
    background-color: rgba(0,0,0, 0.75);
    position: absolute;
    transition: transform .04s linear, opacity 1.5s;
    transform-origin: 50% 50%;
    filter: brightness(1.4) blur(0.3px);
  }
  .thys.interacting {
    background-color: rgba(0,0,17, 0.75);
    filter: brightness(0.8) blur(0.5px);
  }
  .thys.killed {
    background-color: rgba(84,0,0, 0.5);
    filter: brightness(1.5) blur(.5px);
  }
  .thys-debug {
    position: absolute;
    margin-top: -2px;
    margin-left: -2px;
    width: 2px;
    height: 2px;
    border: 1px solid red;
    box-shadow: 0 0 8px red;
    border-radius: 50%;
    font-size: 0.6rem;
    white-space: nowrap;
    color: rgba(0,0,0,0.25);
  }
  `;

  document.body.append(style);

  var container = document.createElement('div');
  container.className = 'thyses';
  document.body.appendChild(container);

  window.onscroll = function() {
    // var top  = window.pageYOffset || document.documentElement.scrollTop;
    // var left = window.pageXOffset || document.documentElement.scrollLeft;
    // container.style.top = top + 'px';
  };

    window.requestAnimationFrame = window.requestAnimationFrame
        || window.mozRequestAnimationFrame
        || window.webkitRequestAnimationFrame
        || window.msRequestAnimationFrame
        || function(f){return setTimeout(f, 1000/60)} // simulate calling code 60

      window.cancelAnimationFrame = window.cancelAnimationFrame
        || window.mozCancelAnimationFrame
        || function(requestID){clearTimeout(requestID)} //fall back

    // Vec2 class.
    function Vec2(x, y) {
        this.x = x;
        this.y = y;
    }
    Vec2.prototype.getDistance = function(vec2) {
        var a = this.x - vec2.x;
        var b = this.y - vec2.y;

        return Math.sqrt( a*a + b*b );
    };
    // Get angle in degrees (see: https://gist.github.com/conorbuck/2606166)
    Vec2.prototype.getAngle = function(vec2) {
        var x = vec2.x - this.x;
        var y = vec2.y - this.y;

        return Math.atan2(y, x) * 180 / Math.PI;
    }
  Vec2.prototype.toString = function() { return this.x + 'x' + this.y; };

  // ParticleSystem class.
  function ParticleSystem() {
    this.particles = [];
  }
  ParticleSystem.prototype.add = function(p) {
    this.particles.push(p);
  };
  ParticleSystem.prototype.update = function() {};
  ParticleSystem.prototype.get = function(i) {
    return typeof this.particles[i] != 'undefined' ? this.particles[i] : null;
  };
  ParticleSystem.prototype.getAll = function() { return this.particles; };
  ParticleSystem.prototype.count = function() { return this.particles.length; };
  // Get particles near from the given one.
  ParticleSystem.prototype.nearest = function(p, distance) {
    if (typeof distance == 'undefined') distance = 50;

    let nearest = [];

    for (let i = 0; i < this.count(); i++) {
      let _p = this.get(i);
      if (_p == p) continue;
      if (p.isDead() || _p.isDead()) continue;
      let dist = p.getDistance(_p);
      if (dist <= distance) {
        nearest.push(_p);
        p.interact(_p, dist);
      }
    }

    return nearest;
  };

    // Thysanoptera class.
  function Thysanoptera(ctn) {
    this.container = ctn || document.body;
    this.init();
  }

  Thysanoptera.counter = 0;

  Thysanoptera.prototype.init = function() {
    Thysanoptera.counter++;
    let self = this;
    this.id = Thysanoptera.counter;
    this.size = 1;

    // Body.
    this.body = document.createElement('div');
    this.body.className = 'thys';

    // Debug.
    if (DEBUG) {
      this.debug = document.createElement('div');
      this.debug.className = 'thys-debug';
    }

    this._state = {
      moving: false,
      dead: false,
      position: new Vec2(0, 0),
      rotation: 0,
      targetPosition: new Vec2(0, 0),
      lastTick: new Date().getTime()
    };
    this.setNewPosition();
    this.setNewTarget();

    this.container.appendChild(this.body);
    if (DEBUG) {
      this.container.appendChild(this.debug);
    }

    this.__waketimeout = null;
    this.body.self = function () { return self; };
    // Allows to stop the thys using <DOM element>.stop() method:
    this.body.stop = this.stop.bind(this);
    this.body.kill = this.kill.bind(this);
    this.body.onclick = this.kill.bind(this);
    this.body.state = function() { return self._state; };
  };
  Thysanoptera.prototype.kill = function() {
    this.stop();
    this._state.dead = true;
    this.body.classList.add('killed');
  };
  Thysanoptera.prototype.undead = function () {
    this._state.dead = false;
    this.body.classList.remove('killed');
    this.move();
  };
  Thysanoptera.prototype.improve = function() {
    this.size++;
    this.setNewTarget();
    this.body.setAttribute('data-size', this.size);
  };

  Thysanoptera.prototype.tick = function() {
    let t = new Date().getTime();
    let tl = t - this._state.lastTick;
    this._state.lastTick = t;
    return tl;
  };
  Thysanoptera.prototype.isMoving = function() { return this._state.moving; };
  Thysanoptera.prototype.isDead = function() { return this._state.dead; };
  Thysanoptera.prototype.move = function() { this._state.moving = true; };
  Thysanoptera.prototype.stop = function() { this._state.moving = false; };
// Get distance between this thys and another one.
  Thysanoptera.prototype.getDistance = function(part2) { return this.getPosition().getDistance(part2.getPosition()); };
  Thysanoptera.prototype.getRotation = function() { return this._state.rotation; };
  Thysanoptera.prototype.getPosition = function() { return this._state.position; };
  Thysanoptera.prototype.getTarget = function() { return this._state.targetPosition; };
  Thysanoptera.prototype.getDistanceFromTarget = function() {
    return this.getPosition().getDistance(this.getTarget());
  };
  Thysanoptera.prototype.getAngleToTarget = function() {
    return this.getPosition().getAngle(this.getTarget());
  };
  Thysanoptera.prototype.getAngleTo = function(part2) {
      return this.getPosition().getAngle(part2.getPosition());
  };
  Thysanoptera.prototype.setNewPosition = function() {
    this._state.position = new Vec2(
      Math.floor(Math.random() * this.container.offsetWidth),
      Math.floor(Math.random() * this.container.offsetHeight)
    );
  };
  Thysanoptera.prototype.setNewTarget = function(vec) {
    if (typeof vec == 'undefined') {
      this._state.targetPosition = new Vec2(
        Math.floor(Math.random() * this.container.offsetWidth),
        Math.floor(Math.random() * this.container.offsetHeight)
      );
    } else {
      this._state.targetPosition = vec;
    }

    this._state.rotation = this.getAngleToTarget();

    this.body.setAttribute('data-target', this.getTarget());

    // Update debug.
    if (DEBUG) {
      this.debug.style.top = this._state.targetPosition.y + 'px';
      this.debug.style.left = this._state.targetPosition.x + 'px';
      this.debug.textContent = this.getTarget() + ' - R: ' + this._state.rotation;
    }

    //console.log(`id: ${this.id} -> NEW POS: ${this.getTarget()} - ROT: ${this._state.rotation}`);
  };
  Thysanoptera.prototype.interact = function(part2, distance) {
    if (distance < 7) {
      if (this.size > part2.size) {
        part2.kill();
        this.improve();
      } else {
        part2.improve();
        this.kill();
      }
    }
    // Try to escape from bigger particles.
    else if (distance <= 30) {
      if (this.size < part2.size) {
        this.setNewTarget();
      } else {
        // @todo: Go to part2 target position.
      }
    }
    //this.body.classList.add('interacting');
    //part2.body.classList.add('interacting');
    // Look at other part direction.
    //this._state.rotation = this.getAngleTo(part2);

    //this.__waketimeout = setTimeout(
  };
  Thysanoptera.prototype.update = function() {
      //console.log(`id: ${this.id} -> state: ${this._state} - POS: ${this.getTarget()} - ROT: ${this._state.rotation}`);
    if (this.isDead()) return;

    let tl = this.tick();
    let fps = 1000 / tl;

    let posx = this.getPosition().x;
    let posy = this.getPosition().y;
    // let _rot = (Math.random() * this.getRotation() * .2);
    // _rot = this.getRotation() - (_rot * .5);
    let _rot = this.getRotation();
    let _rotRad = parseFloat(_rot) * (Math.PI / 180)
    let _size = 1 + (this.size * .1);

    if (this.isMoving()) {
      //console.log(`id: ${this.id} -> tick: ${tl}`);

      // Manage limits.
      if (this.getPosition().x > this.container.offsetWidth
        || this.getPosition().y > this.container.offsetHeight
        || this.getPosition().x < 0
        || this.getPosition().y < 0) {
        this.setNewPosition();
        this.setNewTarget();
      }

      // Move.
      let dist = this.getDistanceFromTarget();
      let _speed = (Math.random() * speed * .25);
      _speed = speed - (_speed * .5);

      if (this.getDistanceFromTarget() < 3) {
        this.setNewTarget();
      }

      // Calculates new position based on angle.
      let _stepX = (_speed / fps) * Math.cos(_rotRad);
      let _stepY = (_speed / fps) * Math.sin(_rotRad);
      this.getPosition().x = this.getPosition().x + _stepX;
      this.getPosition().y = this.getPosition().y + _stepY;

      posx = this.getPosition().x;
      posy = this.getPosition().y;

      // Add some rotation randomness.
      _rot = (Math.random() * this.getRotation() * .2);
      _rot = this.getRotation() - (_rot * .5);
    } else {
      _rot = (Math.random() * this.getRotation() * .4);
      _rot = this.getRotation() - (_rot * .5);
    }

    this.body.style.transform = `translateX(${posx}px) translateY(${posy}px) rotate(${_rot}deg) scale(${_size})`;
    this.body.style.transformOrigin = '50% 50%';
    this.body.style.opacity = '1';
  };

  let frame = 0;
  let last = 0;
  let elapsed = 0;
  const spawning_frames = 60;

  window.__parsys = new ParticleSystem();

  function render(now) {
    let diff = now - last;
    last = now;
    fps = 1000 / diff;

    // console.log(frame, elapsed, fps);

    // Spawn a new thrips every given frames.
    if (elapsed >= spawning_frames) {
      // Do animation.
      //console.log('> ', frame);

            // Add some new particles :-D
      if (__parsys.count() < max_number) {
        let thys = new Thysanoptera(container);
        __parsys.add(thys);
        thys.move();
      }

      elapsed = 0;
    }

  // Update all particles.
  for (let i in __parsys.getAll()) {
    let p = __parsys.get(i);
    // Look for interaction.
    __parsys.nearest(p);
    // Update particle.
    p.update();
  }

    window.requestAnimationFrame(render);

    elapsed++;
    frame++;
  }

  // Add action on container click.
  document.body.addEventListener('click', function(e) {
    let target = e.target;

    if (target.nodeName != 'A') {
      console.log(e);

      // Mouse vector.
      let vec = new Vec2(e.x, e.y);

      // Update all particles.
      for (let i in __parsys.getAll()) {
        let p = __parsys.get(i);
        p.setNewTarget(vec);
      }

      e.preventDefault();
      return false;
    }
  }, {passive: false});

  window.requestAnimationFrame(render);

  for (let i=0; i < number; i++) {
    let thys = new Thysanoptera(container);
    __parsys.add(thys);
    thys.move();
  }

})(window);
