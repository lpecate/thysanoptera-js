# Thrips JS

Thrips (order Thysanopteras) are really tiny (mostly 1 mm long or less) slender insects. Thrips may invade houses and infest household objects such as furniture, bedding and computer monitors - in the latter case by forcing their way in between the LCD and its glass covering.

This script aims to simulate thrips between LCD monitor to made people crazy.

## Bookmarklet

```
javascript:(function(){s=document,o='script',g='https://gitlab.com/lpecate/thysanoptera-js/-/raw/main/thys.js',a=s.createElement(o);a.async=1;a.src=g;s.body.appendChild(a)})();
```
